﻿open System

let WorldWidth = 6
let WorldHeight = 6
let ThugCount = 10

let Sink(text:string) : unit = Console.WriteLine(text)

type Thug =
    {
        X:int
        Y:int
    }

let CreateThug(random:Random) : Thug =
    {
        X = random.Next(WorldWidth)
        Y = random.Next(WorldHeight)
    }

type World =
    {
        X: int
        Y: int
        Thugs: Set<Thug>
    }

let HasThug (x:int, y:int) (world:World): bool =
    world.Thugs
    |> Seq.tryFind (fun t -> t.X=x && t.Y=y)
    |> Option.map (fun _ -> true)
    |> Option.defaultValue false

let RemoveThug (x:int, y:int) (world:World): World =
    let thugs = 
        world.Thugs
        |> Set.filter (fun thug -> thug.X <> x || thug.Y<> y)
    {world with Thugs = thugs}


let rec GenerateThugs(world:World) : World =
    if world.Thugs.Count=ThugCount then
        world
    else
        GenerateThugs {world with Thugs = world.Thugs |> Set.add (CreateThug(Random()))}

let CreateWorld(): World =
    {
        World.X=WorldWidth/2
        Y=WorldHeight/2
        Thugs=Set.empty
    }
    |> GenerateThugs

type Direction =
    | North
    | East
    | South
    | West

type Command =
    | Quit
    | Move of Direction
    | Ride

let  ParseCommand (tokens: string list) : Command option =
    match tokens with
    | ["quit"] -> Quit |> Some
    | ["north"] -> North |> Move |> Some
    | ["east"] -> East |> Move |> Some
    | ["south"] -> South |> Move |> Some
    | ["west"] -> West |> Move |> Some
    | ["ride";"horses"] -> Ride |> Some
    | _ -> None

let Move(direction:Direction) (world:World) : World =
    match direction, world.X, world.Y with
    | West, 0, _ -> world
    | East, x, _ when x=(WorldWidth-1) -> world
    | South, _, y when y=(WorldHeight-1) -> world
    | North, _, 0 -> world

    | North, _, _ ->
        {world with Y = world.Y - 1}
    | East, _, _ ->
        {world with X = world.X + 1}
    | South, _, _ ->
        {world with Y = world.Y + 1}
    | West, _, _ ->
        {world with X = world.X - 1}

let RideHorses(world:World) : World =
    if HasThug(world.X,world.Y) world then
        "You ride horses with the thug for a while, then he wanders off." |> Sink
        world |> RemoveThug (world.X,world.Y)
    else
        "There are no thugs to ride horses with!" |> Sink
        world

let rec Loop(world:World):unit =
    "" |> Sink
    world.X |> sprintf "X=%d" |> Sink
    world.Y |> sprintf "Y=%d" |> Sink
    if world.Thugs.IsEmpty then
        "You have made a name for yerself!" |> Sink
    if HasThug(world.X,world.Y) world then
        "There is a thug here." |> Sink
    "What do you want to do?" |> Sink
    Console.Write ">"
    match Console.ReadLine().ToLower().Split([|' '|]) |> List.ofArray |> ParseCommand with
    | Some Ride -> Loop (world |> RideHorses)
    | Some (Move direction) -> Loop (world |> Move direction)
    | Some Quit -> ()
    | _ -> Loop world

[<EntryPoint>]
let main argv =
    "Commands:" |> Sink
    "\tnorth - moves north" |> Sink
    "\teast - moves east" |> Sink
    "\tsouth - moves south" |> Sink
    "\twest - moves west" |> Sink
    "\tride horses - rides horses, when a thug is present" |> Sink
    "\tquit - quits the game" |> Sink
    CreateWorld()
    |> Loop
    0
